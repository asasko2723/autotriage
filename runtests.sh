#!/bin/bash

PATH=$PATH:./triagewebserver/tests/drivers:$HOME/.local/bin/

coverage run --source="triagewebserver" manage.py test triagewebserver/tests
coverage xml
