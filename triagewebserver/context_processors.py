from django.conf import settings
from triagewebserver.models import Organization
from django.contrib.auth import get_user_model

def global_context(request):

    if request.user.is_authenticated:
        currentOrg = Organization.objects.filter(id = request.session['organization_id']).first()
    else:
        currentOrg = ''

    return {
        'SITE_TITLE': settings.SITE_TITLE,
        'current_organization': currentOrg
    }
