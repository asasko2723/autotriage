from django.test import TestCase
from django.urls import reverse
from triagewebserver.models import User

class TestViews(TestCase):

    def test_index_returns_index_template_and_returns_200_status_code(self):
        response = self.client.get(reverse('index'))
        self.assertTemplateUsed(response, 'index.html')

    def test_proflie_redirects_if_unauthenticated(self):
        response = self.client.get(reverse('profile'))
        self.assertRedirects(response, '/accounts/login/')

    def test_proflie_returns_profile_template_if_authenticated(self):
        user = User.objects.create_user('demo', 'demo@demo.com', 'P@ssw0rd')
        user.save()
        self.client.force_login(user)
        response = self.client.get(reverse('profile'))
        self.assertTemplateUsed(response, 'profile.html')
