"""triagewebserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.index, name='index')
Class-based views
    1. Add an import:  from other_app.views import index
    2. Add a URL to urlpatterns:  path('', index.as_view(), name='index')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as authViews
from django.urls import path, include#, re_path

from triagewebserver import views

urlpatterns = [
    # Admin Page
    path('admin/', admin.site.urls),

    # Index
    path('', views.index, name='index'),

    # Dashboard
    path('dashboard/', views.dashboard, name='dashboard'),

    # Bed Groups
    path('bed-groups/', views.bedGroups, name='bed-groups'),
    path('bed-groups/add', views.addBedGroups, name='addBedGroup'),

    # Beds
    path('beds/', views.beds, name='beds'),
    path('beds/add', views.addBeds, name='addBeds'),

    # Rule Groups
    path('rule-groups/', views.ruleGroups, name='rule-groups'),
    path('rule-groups/add', views.addRuleGroups, name='addRuleGroup'),

    # Rules
    path('rules/', views.rules, name='rules'),
    path('rules/add', views.addRules, name='addRules'),

    #Rule to Rule group mappings
    path('rule-groups-rule/', views.rulegrouprules, name='rule-groups-rule'),
    path('rule-groups-rule/add', views.addrulegrouprules, name='add-rule-groups-rule'),

    #Rule bed mappings
    path('rulebedmappings/', views.rulebedmapping, name='rulebedmapping'),
    path('rulebedmappings/add', views.addrulebedmapping, name='addrulebedmapping'),

    #Rule bed mappings
    path('api', views.api, name='api'),


    # Organization
    # path('organization/', views.organization, name='organization'),

    # Organizations
    path('organizations/', views.organizations, name='organizations'),
    path('organizations/update/<uuid:organization_id>', views.organizationChange, name='organizationChange'),
    path('organizations/add', views.addOrganization, name='addOrganization'),

    # Profile
    path('profile/', views.profile, name='profile'),

    # Account Registration
    path('register/', views.RegisterUser.as_view(), name='register'),
    path('confirm/<uuid:confirmation_token>/', views.accountConfirmation, name='accountConfirmation'),

    # Overloaded login
    path('login/', views.LoginView.as_view(), name='login'),

    # Default Auth Views
    path('logout/', authViews.LogoutView.as_view(), name='logout'),
    path('password_change/', authViews.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', authViews.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('password_reset/', authViews.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', authViews.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', authViews.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', authViews.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]

admin.site.site_header = 'TriageWebServer Administration'
admin.site.site_title = 'TriageWebServer'
admin.site.index_title = 'TriageWebServer Administration'
