from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags



# Email Send Templates
def send_basic(email_content):
    send_mail(
        email_content.get("subject"),
        email_content.get("message"),
        email_content.get("sender"),
        email_content.get("recipients"),
        fail_silently=False,
        # TODO: make this true in production
    )

def send_html(emailContent, context):
    print(context["confirmation_token"])
    html_content = render_to_string(emailContent.get("templatePath"), {'context' : context}) # render with dynamic value
    text_content = strip_tags(html_content) # Strip the html tag. So people can see the pure text at least.

    # create the email, and attach the HTML version as well.
    email = EmailMultiAlternatives(
        emailContent.get("subject"),
        text_content, emailContent.get("sender"),
        emailContent.get("recipients")
    )
    email.attach_alternative(html_content, "text/html")
    email.send()



# Emails
def accountConfirmationEmail(user):
    context = {
        'confirmation_token' : user.confirmation_token
    }

    emailContent = {
        "subject"        : "Autotriage - Account Confirmation",
        "sender"         : "noreply@autotriage.com",
        "recipients"     : [user.email],
        "templatePath"   : "emails/confirmationEmail.html",
    }

    send_html(emailContent, context)

def welcome():
    email_body = "BOI this is a test"
    email_content = {
        "subject"    : "Django Email Test",
        "sender"     : "noreply@autotriage.com",
        "recipients" : ["testboi@memes.com"],
        "message"    : email_body
    }
    send_basic(email_content)
