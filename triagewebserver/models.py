import uuid
from django.db import models
from django.contrib.auth.models import Group, AbstractUser
from django.utils import timezone
from enum import Enum

class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    register_ip = models.GenericIPAddressField(default='192.168.0.1')
    lastip = models.GenericIPAddressField(default='192.168.0.1')
    confirmation_token = models.UUIDField(default=uuid.uuid4, editable=True, null=True, blank=True)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)
    email = models.CharField(max_length=40, unique=True)
    username = models.CharField(unique=False, null=True, blank=True, max_length=40)
    is_active = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['']

class User_Settings(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    key = models.CharField(max_length=300)
    value = models.CharField(max_length=300)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Organization(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300)
    key = models.CharField(max_length=300)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Organization_Users(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Organization_Profile_Mapping(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    profile = models.ForeignKey(Group, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Organization_Settings(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300)
    key = models.CharField(max_length=300)
    value = models.CharField(max_length=300)
    description = models.TextField()
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Bed_group(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    description = models.TextField()
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Bed(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bed_group = models.ForeignKey(Bed_group, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    status = models.IntegerField(default=0)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Triage_attributes(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    key = models.CharField(max_length=300)
    value = models.CharField(max_length=300)
    description = models.TextField()
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Bed_Group_attributes(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bed_group = models.ForeignKey(Bed_group, on_delete=models.CASCADE)
    attribute = models.ForeignKey(Triage_attributes, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Bed_attributes(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bed = models.ForeignKey(Bed, on_delete=models.CASCADE)
    attribute = models.ForeignKey(Triage_attributes, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Patient(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    identifier = models.CharField(max_length=500)
    esi = models.CharField(max_length=300)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    primary_symptom = models.CharField(max_length=300)
    bodily_system = models.CharField(max_length=300)
    other = models.CharField(max_length=500)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Assignments(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    org = models.ForeignKey(Organization, on_delete=models.CASCADE)
    bed_group = models.ForeignKey(Bed_group, on_delete=models.CASCADE)
    bed = models.ForeignKey(Bed, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Audit_log(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    action = models.TextField()
    data = models.TextField()
    user = models.TextField()
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class System_Settings(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    key = models.CharField(max_length=300)
    value = models.CharField(max_length=300)
    description = models.TextField()
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class RuleTypes(Enum):   # A subclass of Enum
    PS = "Primary Symptom"
    ESI = "ESI"
    BS = "Bodily System"
    OT = "Other"

class Rule(models.Model): #esi level, symptom, triage stuff
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300)
    value = models.CharField(max_length=300)
    description = models.CharField(max_length=300)
    rule_type = models.CharField(
        max_length=3,
        choices=[(tag, tag.value) for tag in RuleTypes]  # Choices is a list of Tuple
    )
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Rule_Group(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=300)
    description = models.CharField(max_length=300)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Rule_Group_Mapping(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rule = models.ForeignKey(Rule, on_delete=models.CASCADE)
    group = models.ForeignKey(Rule_Group, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)

class Rule_Bed_Mappings(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rule_group = models.ForeignKey(Rule_Group, on_delete=models.CASCADE)
    bed_group = models.ForeignKey(Bed_group, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    priority = models.IntegerField()
    createdDate = models.DateTimeField(default=timezone.now)
    lstUpdDate = models.DateTimeField(auto_now=True)
