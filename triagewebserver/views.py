from django import forms
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import get_object_or_404
from django.contrib.auth import login
from django.utils.crypto import get_random_string
from triagewebserver.email_templates import *
from triagewebserver.models import *

class AuthenticationForm(AuthenticationForm):
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.
        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.
        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )


class RegisterUserForm(UserCreationForm):
    email = forms.EmailField(required=True, label='Email', error_messages=
                             {'exists': 'User already exists with email'})
    organization = forms.CharField(required=True, label='Organization')

    class Meta:
        model = get_user_model()
        fields = ('email', 'organization', 'password1', 'password2')

    def save(self, commit=True):
        user = super(RegisterUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        organization_name = self.cleaned_data['organization']
        if commit:
            user.save()
            organization_find = Organization.objects.create(name=organization_name)
            Organization_Users.objects.create(organization=organization_find, user=user)
            accountConfirmationEmail(user)
        return user

    def clean_email(self):
        if get_user_model().objects.filter(email=self.cleaned_data['email']).exists():
            raise ValidationError(self.fields['email'].error_messages['exists'])

        return self.cleaned_data['email']

class UserProfileChangeForm(UserChangeForm):
    password = None

    class Meta:
        model = get_user_model()
        fields = ('username', 'email')

class RegisterUser(generic.CreateView):
    form_class = RegisterUserForm
    success_url = reverse_lazy('login')
    template_name = 'registration/register.html'

class LoginForm(LoginView):
    form_class = AuthenticationForm

def index(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            #client = nexmo.Client(application_id=getattr(settings, "NEXMO_APPLICATION_ID"), \
            #private_key=getattr(settings, "NEXMO_PRIVATE_KEY"))
            #calls = client.get_calls().get("_embedded")
            #calls = calls.get("calls")
            #calls = Call.objects.all().order_by("-timestamp")
            print("hello")
        else:
            print("goodbye")

    return render(request, 'index.html')

def accountConfirmation(request, confirmation_token):
    if request.method == 'GET':
    # TODO: Clean this data input maybe?

        # return 404 if token is expired or invalid
        user = get_object_or_404(User, confirmation_token=confirmation_token)

        # Invalidate the token by clearing it and set the account as active
        user.confirmation_token = None
        user.is_active = 1
        user.save()

        # login user and redirect to dashboard
        login(request, user)
        return redirect('dashboard')

    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)



#dashboard Logic
def dashboard(request):
    if request.user.is_authenticated:
        if request.session.get('organization_id') is None:
            request.session['organization_id'] = str\
                (Organization_Users.objects.filter(user=request.user.id).first().organization.id)
        organization_bed_groups = Bed_group.objects.filter(organization=\
            request.session['organization_id']).order_by('-createdDate')[0:3]
        organization_patients = Patient.objects.filter(organization=\
            request.session['organization_id']).order_by('-createdDate')[0:3]
        assignments = Assignments.objects.filter(org=\
            request.session['organization_id']).order_by('-createdDate')[0:10]
        beds = Bed.objects.filter(status=0)
        return render(request, 'dashboard/triaging/triaging-live.html', \
            {'organization_bed_groups':organization_bed_groups, 'organization_patients' \
                : organization_patients, 'assignments' : assignments, 'beds' : beds})
    else:
        return redirect('login')



#Bed Group Logic
def bedGroups(request):
    if request.user.is_authenticated:
        organization_bed_groups = Bed_group.objects.filter(organization=\
            request.session['organization_id'])
        return render(request, 'dashboard/bed_groups/bed_groups-index.html', \
            {'organization_bed_groups': organization_bed_groups})
    else:
        return redirect('login')

def addBedGroups(request):
    i = 0
    if request.user.is_authenticated and request.method == 'POST':
        # Create organization instance from Current Organization id
        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()

        # Create New Bed Group
        new_bed_group = Bed_group.objects.create(organization=current_organization, \
            name=request.POST['name'], description=request.POST['desc'])


        # Add the number of beds specified to the bed group
        for new_bed in range(int(request.POST['new_bed_count'])):
            i += 1
            bed_name = "Bed " + str(i) + " for " + new_bed_group.name
            Bed.objects.create(bed_group=new_bed_group, name=bed_name)

        return bedGroups(request)

    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)



# Beds Logic
def beds(request):
    if request.user.is_authenticated:
        # Create organization instance from Current Organization id
        #curent_organization = Organization.objects.filter(id = \
        # request.session['organization_id']).first()

        # Get list of Bed Groups
        organization_bed_groups = Bed_group.objects.filter\
            (organization=request.session['organization_id']).order_by('name')

        # Get list of Beds in the Organization's Bed_Groups
        organization_beds = []
        for organization_bed_group in organization_bed_groups:
            organization_beds += Bed.objects.filter(bed_group_id=organization_bed_group)

        return render(request, 'dashboard/beds/beds-index.html', {'organization_beds'\
            : organization_beds, 'organization_bed_groups': organization_bed_groups})
    else:
        return redirect('login')

def addBeds(request):
    i = 0
    if request.user.is_authenticated and request.method == 'POST':

        # Get Bed_Group instance from requested Bed_Group id
        bed_group = Bed_group.objects.filter(id=request.POST['bed_group_assignment']).first()
        # Add the number of beds specified to the bed group
        for new_bed in range(int(request.POST['new_bed_count'])):
            i += 1
            bed_name = "Bed " + str(i) + " for " + bed_group.name
            Bed.objects.create(bed_group=bed_group, name=bed_name)

        return beds(request)
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)



# Organization Logic
def organization(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            user_organizations = Organization.objects.all().order_by("-id")
        else:
            user_organizations = Organization_Users.objects.filter(user=request.user)
        return render(request, \
            'dashboard/organization.html', {'user_organizations': user_organizations})
    else:
        return redirect('login')

def addOrganization(request):
    if request.user.is_authenticated and request.method == 'POST':
        unique_id = get_random_string(length=64, \
            allowed_chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')

        # Create New organization and add current user as a mebember
        new_organization = Organization.objects.create(name=request.POST['name'], key=unique_id)

        # Add Current user to the newly created Organization
        Organization_Users.objects.create(organization=new_organization, user=request.user)

        return organization(request)
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)

def organizations(request):
    if request.user.is_authenticated:
        user_organizations = Organization_Users.objects.filter(user=request.user)
        return render(request, \
            'dashboard/organizations.html', {'user_organizations' : user_organizations})
    else:
        return redirect('login')

def organizationChange(request, organization_id):
    if request.user.is_authenticated:
        request.session['organization_id'] = str(organization_id)
        return redirect('dashboard')
    else:
        return redirect('login')

def profile(request):
    if not request.user.is_authenticated:
        return redirect('login')

    if request.method == 'POST':
        form = UserProfileChangeForm(request.POST, instance=request.user)
        print(form.errors)
        if form.is_valid():
            for field in form.cleaned_data:
                if form.cleaned_data[field] != getattr(request.user, field):
                    setattr(request.user, field, form.cleaned_data[field])
                    request.user.save()
                else:
                    form = UserProfileChangeForm(instance=request.user)
    else:
        form = UserProfileChangeForm(instance=request.user)
    return render(request, 'profile.html', {'form': form})



def getFirstOrganization(request):
    organization_user = Organization_Users.objects.filter(user=request.user)
    organization_obj = Organization.objects.get(id=organization_user.organization.id)
    return organization_obj


def rules(request):
    if request.user.is_authenticated:
        # Create organization instance from Current Organization id
        #curent_organization = Organization.objects.filter\
        # (id = request.session['organization_id']).first()
        # Get list of Bed Groups
        org_rules = Rule.objects.filter\
            (organization=request.session['organization_id']).order_by('name')
        return render(request, 'dashboard/rules/rules-index.html', {'organization_rules'\
            : org_rules})
    else:
        return redirect('login')

def addRules(request):
    if request.user.is_authenticated and request.method == 'POST':

        # Create organization instance from Current Organization id
        org_rules = Rule.objects.filter\
            (organization=request.session['organization_id']).order_by('name')

        # Create New rule 
        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()
        Rule.objects.create(organization=current_organization, \
            name=request.POST['name'], description=request.POST['desc'], \
                rule_type=request.POST['rule_type_assignment'], value=request.POST['value'])

        return render(request, 'dashboard/rules/rules-index.html', {'organization_rules'\
            : org_rules})
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)

def ruleGroups(request):
    if request.user.is_authenticated:
        # Create organization instance from Current Organization id
        #curent_organization = Organization.objects.filter\
        # (id = request.session['organization_id']).first()

        # Get list of Bed Groups
        org_rule_groups = Rule_Group.objects.filter\
            (organization=request.session['organization_id']).order_by('name')
        return render(request, 'dashboard/rule_groups/rule_groups-index.html', \
            {'organization_rules_groups' : org_rule_groups})
    else:
        return redirect('login')

def addRuleGroups(request):
    if request.user.is_authenticated and request.method == 'POST':
        # Create organization instance from Current Organization id
        org_rule_groups = Rule_Group.objects.filter\
            (organization=request.session['organization_id']).order_by('name')

        # Create New Rule Group
        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()
        Rule_Group.objects.create(organization=current_organization, \
            name=request.POST['name'], description=request.POST['desc'])
        return render(request, 'dashboard/rule_groups/rule_groups-index.html',\
             {'organization_rules_groups' : org_rule_groups})
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)


def rulegrouprules(request):
    if request.user.is_authenticated:
        # Create organization instance from Current Organization id
        #curent_organization = Organization.objects.filter\
        # (id = request.session['organization_id']).first()

        # Get list of Bed Groups
        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()
        org_rule_groups = Rule_Group.objects.filter\
            (organization=current_organization).order_by('name')
        org_rules = Rule.objects.filter\
            (organization=current_organization).order_by('name')
        org_rule_mappings = Rule_Group_Mapping.objects.filter\
            (organization=current_organization).order_by('id')



        return render(request, 'dashboard/rule_group_mappings/rule_group-mappings-index.html', \
            {'organization_rules' : org_rules, 'organization_rule_groups' : org_rule_groups,\
                 'organization_rule_mappings' : org_rule_mappings})
    else:
        return redirect('login')

def addrulegrouprules(request):
    if request.user.is_authenticated and request.method == 'POST':
        # Create organization instance from Current Organization id
        post_rule = Rule.objects.filter(id=request.POST['rule']).first()
        post_rule_group = Rule_Group.objects.filter(id=request.POST['rule_group']).first()

        # Create New Rule Group
        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()



        Rule_Group_Mapping.objects.create(organization=current_organization, \
            rule=post_rule, group=post_rule_group)
        org_rule_groups = Rule_Group.objects.filter\
            (organization=current_organization).order_by('name')
        org_rules = Rule.objects.filter\
            (organization=current_organization).order_by('name')
        org_rule_mappings = Rule_Group_Mapping.objects.filter\
            (organization=current_organization).order_by('id')
        return render(request, 'dashboard/rule_group_mappings/rule_group-mappings-index.html', \
            {'organization_rules' : org_rules, 'organization_rule_groups' : org_rule_groups,\
                 'organization_rule_mappings' : org_rule_mappings})
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)

def rulebedmapping(request):
    if request.user.is_authenticated:
        # Create organization instance from Current Organization id
        #curent_organization = Organization.objects.filter\
        # (id = request.session['organization_id']).first()

        # Get list of Bed Groups
        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()
        org_rule_groups = Rule_Group.objects.filter\
            (organization=current_organization).order_by('name')
        org_bed_groups = Bed_group.objects.filter\
            (organization=current_organization).order_by('name')
        org_mappings = Rule_Bed_Mappings.objects.filter\
            (organization=current_organization).order_by('priority')

        return render(request, 'dashboard/bed_group_rule_mappings/bed-group-rule-mappings-index.html',\
             {'organization_rule_groups' : org_rule_groups, \
                 'organization_bed_groups' : org_bed_groups, 'organization_mappings' : org_mappings})
    else:
        return redirect('login')


def addrulebedmapping(request):
    if request.user.is_authenticated and request.method == 'POST':

        current_organization = Organization.objects.filter(\
            id=request.session['organization_id']).first()
        post_rule_group = Rule_Group.objects.filter(id=request.POST['rule_group']).first()
        post_bed_group = Bed_group.objects.filter(id=request.POST['bed_group']).first()

        Rule_Bed_Mappings.objects.create(rule_group=post_rule_group, bed_group=post_bed_group, \
            organization=current_organization, priority=request.POST['priority'])

        # Create New Rule Group
        org_rule_groups = Rule_Group.objects.filter\
            (organization=current_organization).order_by('name')
        org_bed_groups = Bed_group.objects.filter\
            (organization=current_organization).order_by('name')
        org_mappings = Rule_Bed_Mappings.objects.filter\
            (organization=current_organization).order_by('priority')
        return render(request, 'dashboard/bed_group_rule_mappings/bed-group-rule-mappings-index.html',\
             {'organization_rule_groups' : org_rule_groups, 'org_bed_groups' : org_bed_groups, 'organization_mappings' : org_mappings})
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)

def findAvailableBed(bedgroup):
    first_available_bed = Bed.objects.filter(bed_group=bedgroup, status=0).first()
    first_available_bed.status = 1
    first_available_bed.save()
    return first_available_bed

def processPatient(patient):
    winningcount = 0
    all_org_rule_groups = Rule_Bed_Mappings.objects.filter\
        (organization=patient.organization).order_by("priority")
    for mappings in all_org_rule_groups:
        rule_group = mappings.rule_group
        for each_mapping in Rule_Group_Mapping.objects.filter(group=rule_group):
            match = 0
            rule = Rule.objects.filter(id=each_mapping.rule.id).first()
            if rule.rule_type == "PS":
                matching_ps = Rule.objects.filter\
                    (rule_type="PS", value=patient.primary_symptom, \
                        organization=patient.organization).count()
                if matching_ps > 0:
                    match += 1
            elif rule.rule_type == "ESI":
                matching_esi = Rule.objects.filter\
                    (rule_type="ESI", value=patient.esi, organization=patient.organization).count()
                if matching_esi > 0:
                    match += 1
            elif rule.rule_type == "BS":
                matching_bs = Rule.objects.filter\
                    (rule_type="BS", value=patient.bodily_system, organization=patient.organization).count()
                if matching_bs > 0:
                    match += 1
            elif rule.rule_type == "OT":
                matching_other = Rule.objects.filter\
                    (rule_type="OT", value=patient.other, organization=patient.organization).count()
                if matching_other > 0:
                    match += 1
            else:
                print("not found")
            if match == 4:
                winningcount = 4
                currentwinner = mappings
                break
            elif match > 0 and match > winningcount:
                winningcount = match
                currentwinner = mappings
            else:
                print("Not a match")
                continue
    print("Patient goes into bed group " + currentwinner.bed_group.name)
    new_assignment = Assignments()
    new_assignment.patient = patient
    new_assignment.org = patient.organization
    new_assignment.bed_group = currentwinner.bed_group
    new_assignment.bed = findAvailableBed(currentwinner.bed_group)
    new_assignment.save()


def api(request):
    org = Organization.objects.filter(id=request.GET['key']).first()
    if org:
        patient = Patient()
        patient.identifier = request.GET['identifier']
        patient.esi = request.GET['esi']
        patient.organization = org
        patient.primary_symptom = request.GET['primary_symptom']
        patient.bodily_system = request.GET['bodily_system']
        patient.other = request.GET['other']
        try:
            patient.save()
        except Exception:
            return HttpResponse("<h1>Accepted Key for Organization: " + org.name +"\
                </h1> <br /> Invalid patient data. Please ensure api key, identifier, esi, primary_symptom, \
                    bodily_system, and other are specified", status=504)
        try:
            processPatient(patient)
        except Exception:
            return HttpResponse("No more beds available", status=503)
        return HttpResponse("", status=204)
    else:
        return HttpResponse("<h1>403 Forbidden</h1>", status=403)
