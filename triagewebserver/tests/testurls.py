import requests
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.phantomjs.webdriver import WebDriver
from django.urls import reverse


class TestUrls(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def crawl(self, url):
        self.selenium.get('%s%s' % (self.live_server_url, url))
        urls = []
        urls.append([x.get_attribute('src') for x in self.selenium.find_elements_by_tag_name('img')])
        urls.append([x.get_attribute('href') for x in self.selenium.find_elements_by_tag_name('link')])
        urls.append([x.get_attribute('src') for x in self.selenium.find_elements_by_tag_name('script')])
        for url_group in urls:
            for url in url_group:
                response = requests.get(url)
                if(response.status_code != 200):
                    print("\nWARNING %s: %s NOT FOUND" % (url, response.status_code), end='')

    def test_index_returns_http_200_status_code(self):
        response = self.client.get(reverse('index'))
        self.crawl(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_register_returns_http_200_status_code(self):
        response = self.client.get(reverse('login'))
        self.crawl(reverse('login'))
        self.assertEqual(response.status_code, 200)

    def test_login_returns_http_200_status_code(self):
        response = self.client.get(reverse('register'))
        self.crawl(reverse('register'))
        self.assertEqual(response.status_code, 200)
